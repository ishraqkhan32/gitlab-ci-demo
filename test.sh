#!/bin/bash

echo "Testing simple continuous verification script"
echo "Executing ./helloworld"

OUTPUT=`./helloworld`
RETVAL=$?

if [ $RETVAL -eq 0 ] ; then
	echo "Return value is 0, PASSED"
else 
	echo "Return value is not 0, FAILED"
	exit 1
fi

if [ "$OUTPUT" == "Hello World" ]; then
	echo "Output is correct, PASSED"
else
	echo "Output is not correct, FAILED"
	exit 1
fi
